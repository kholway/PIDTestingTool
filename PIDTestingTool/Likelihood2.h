#ifndef LIKELIHOOD2_H
#define LIKELIHOOD2_H

#include "TRTFramework/TRTIncludes.h"

class Likelihood2
{
private:
  double sqr( double a ) { return a*a; }
  TString parts[3] = {"BRL","ECA","ECB"};

  double onsetPar_Xe[3][10], onsetPar_Ar[3][10];
  
  TH1* getTH1( TFile *f, TString hname );
  
  TH1 *CALIB_EL_SL[3], *CALIB_EL_TW[3], *CALIB_EL_ZR[3];
  TH1 *CALIB_MU_SL[3], *CALIB_MU_TW[3], *CALIB_MU_ZR[3];
  
public:
   EL::StatusCode initialize( TRT::Config &config );

	 double pHTvP   (int TrtPart, double gamma, double occ, int LHType = 0);
	 double pHTvP_Xe(int TrtPart, double gamma, double occ) { return pHTvP( TrtPart, gamma, occ, 0 ); }
	 double pHTvP_Ar(int TrtPart, double gamma, double occ) { return pHTvP( TrtPart, gamma, occ, 1 ); }


   // Doesn't protect for overlap bins, need to add this functionality.
   double Corr_el_SL(int part, double SL) { return getCorrection( CALIB_EL_SL[part], SL ); } 
   double Corr_el_TW(int part, double TW) { return getCorrection( CALIB_EL_TW[part], TW ); }
   double Corr_el_ZR(int part, double ZR) { return getCorrection( CALIB_EL_ZR[part], ZR ); }
	 
   double Corr_mu_SL(int part, double SL) { return getCorrection( CALIB_MU_SL[part], SL ); }
   double Corr_mu_TW(int part, double TW) { return getCorrection( CALIB_MU_TW[part], TW ); }
   double Corr_mu_ZR(int part, double ZR) { return getCorrection( CALIB_MU_ZR[part], ZR ); }

   double getCorrection( TH1* h, double val );
   double getCorrectionEl( int part, int SL, double TW, double ZR, int code ); 
   double getCorrectionMu( int part, int SL, double TW, double ZR, int code ); 
	 
};

#endif
