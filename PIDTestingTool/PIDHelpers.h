#ifndef PIDHELPERS_H
#define PIDHELPERS_H

#include "TRTFramework/TRTIncludes.h"

namespace TRT { 

  int getStrawLayer( int part, int module, int strawlayer );

  double getZRPosition( const xAOD::TrackParticle *track, const xAOD::TrackMeasurementValidation *DC );

  double getDilepMass( xAOD::IParticleContainer lep ); 

}

#endif // PIDHELPERS_H
