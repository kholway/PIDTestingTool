#include "PIDTestingTool/PIDAnalysis.h"
#include "TRTFramework/TRTIncludes.h"

bool PIDAnalysis::passZSelection( xAOD::IParticleContainer lep )
//--------------------------------------------------------------------------
{
  // >= 2 leptons 
  if ( lep.size() < 2 ) return false;
  
  // OS leptons
  if ( lep[0]->isAvailable<float>("charge") ) {
    if ( lep[0]->auxdata<float>("charge") * lep[1]->auxdata<float>("charge") > 0 ) return false; 
  } else {
    TRT::fatal(" passZSelection expects an electron or muon container");
  }
  
  // Mass Cuts 
  double mll = (lep[0]->p4() + lep[1]->p4()).M() * TRT::invGeV;
  if ( fabs( mll - 91. ) > 10 ) return false;

  return true; 
}


