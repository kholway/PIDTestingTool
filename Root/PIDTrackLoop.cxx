#include "PIDTestingTool/PIDAnalysis.h"
#include "PIDTestingTool/PIDHelpers.h"
#include "TRTFramework/TRTIncludes.h"

void PIDAnalysis::loopOverHits( const xAOD::TrackParticle *track, bool isElectron ) 
{
  // Set Event Weight
  double w = (isData()) ? 1.0 : weight();
  
  // Get Track Parameters
  double trackP = track->p4().P(); 
  
  // Get and plot Track Occupancy
  double OT = getTrackOccupancy(track); // track occupancy 
  histoStore()->fillTH1F( "h_OccTrack", OT, w );
  
  // Print Basic Track Info
  /*
  std::cout << std::endl;
  std::cout << "-----------------------------------------------------------------------------" << std::endl;
  std::cout << "  track Pt = " << track->pt()*TRT::invGeV <<  "   track eta = " << track->eta() << std::endl;
  std::cout << "-----------------------------------------------------------------------------" << std::endl;
  */
  
  // Calculate gamma factor (GF)
  // *** NOTE: Use Pion mass to compare to PIDTool ***  
  double gammaEl = sqrt( trackP*trackP + TRT::massEl*TRT::massEl )/TRT::massEl;
  double gammaMu = sqrt( trackP*trackP + TRT::massMu*TRT::massMu )/TRT::massMu; 
  double GF = ( isElectron ) ? gammaEl : gammaMu;

  // TEMPORARY: Get Mass
  //double mass = ( isElectron ) ? TRT::massEl : TRT::massMu;

  // Count hits and HT hits
  int nTRT(0), nHTMB(0), nHT(0);
  int nHTMB_Ar(0), nHT_Ar(0);
  
  // Prepare for LH calculation
  double LH_El[8] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
  double LH_Mu[8] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
  
  // Get measurements on track 
  typedef std::vector<ElementLink<xAOD::TrackStateValidationContainer>> MeasurementsOnTrack;
  if (not track->isAvailable<MeasurementsOnTrack>("msosLink")) 
    TRT::fatal("No MSOS called 'msosLink' is available on track");
  const MeasurementsOnTrack& measurementsOnTrack = track->auxdata< MeasurementsOnTrack >("msosLink");
 
  // Loop over track hits 
  for ( auto msos_itr: measurementsOnTrack ) {
    if (not msos_itr.isValid()) continue;  

    // skip non-TRT hits
    const xAOD::TrackStateValidation *msos = *msos_itr;
    if( msos->detType() != 3 || msos->type() != 0 ) continue;
   
    // get drift circle 
    if (not msos->trackMeasurementValidationLink().isValid()) continue; 
    const xAOD::TrackMeasurementValidation *TRTDriftCircle =  *(msos->trackMeasurementValidationLink());
	 
    // check is straw is Ar and if HT is passed
    bool isHT    = (TRTDriftCircle->auxdata<char>("highThreshold"));
	  bool isHTMB  = (TRTDriftCircle->auxdata<unsigned int>("bitPattern") & 131072);
   
    int gasType(0);
    bool isArgon(false);
    if ( TRTDriftCircle->isAvailable<char>("isArgon") ) {
      isArgon = (TRTDriftCircle->auxdata<char>("isArgon"));
      gasType = (int) isArgon;
    } else {
      gasType = (TRTDriftCircle->auxdata<char>("gasType"));
      isArgon = (bool) gasType;
    }

    // Count hits and HT hits 
    nTRT++;
    if (isHT) nHT++;
    if (isHTMB) nHTMB++;
    
    if (isHT && isArgon) nHT_Ar++;
    if (isHTMB && isArgon) nHTMB_Ar++;

    // Get partition  
    int TRTpart = 0; // 0: Barrel,  1: Endcap Wheels A,  2: Endcap Wheels B 
    int module = TRTDriftCircle->auxdata<int>("layer");
    if ( abs(TRTDriftCircle->auxdata<int>("bec")) == 2 ) TRTpart = (module < 6) ? 1 : 2;
      //bool sideA = (TRTDriftCircle->auxdata<int>("bec") > 0);

    // Get track-to-wire distance (TW)
	  double TW = fabs( msos->localX() );
    if (TW >= 2.2) TW = 2.175; // can always rebin later...
    
    // Calculate global strawlayer (SL)
    int localSL = TRTDriftCircle->auxdata<int>("strawlayer");
    int SL = TRT::getStrawLayer( TRTpart, module, localSL ); 
    
    // Calculate Z/R position (ZR)
    double ZR = TRT::getZRPosition( track, TRTDriftCircle );

    // Get pHT from onset curves
    //double pHT = LH2()->pHTvP( TRTpart, GF, OT, gasType );
    //double pHT2 = LH()->pHTvP( TRTpart, trackP, mass, OT, gasType );
    //if ( fabs(pHT-pHT2) > 0.000005 ) std::cerr << "Mismatch in pHT = " << pHT << "  " << pHT2 << std::endl;
  
    // Get pHT for electron and muon hypothesis
    double pHTEl = LH2()->pHTvP( TRTpart, gammaEl, OT, gasType );
    double pHTMu = LH2()->pHTvP( TRTpart, gammaMu, OT, gasType );

    // Apply all permutations of CFs to pHT and calculate likelihoods
    double pHT_El[8], pHT_Mu[8];
    for( int i(0); i < 8; i++) { 
      pHT_El[i] = pHTEl * LH2()->getCorrectionEl( TRTpart, SL, TW, ZR, i );
      pHT_Mu[i] = pHTMu * LH2()->getCorrectionMu( TRTpart, SL, TW, ZR, i );

      if (isHTMB) { LH_El[i] *=     pHT_El[i]; LH_Mu[i] *=     pHT_Mu[i];}
      else        { LH_El[i] *= 1.0-pHT_El[i]; LH_Mu[i] *= 1.0-pHT_Mu[i];}
    }

    // Check for short straws
    //bool isShortStraw = ( TRTpart == 0 && SL < 9 ); 

    // Fill pHT plots
    TString hname, suffix;
    bool passGas[3] = { not isArgon, isArgon, true };
    for ( int ig(0); ig < 3; ig++ ) {
      if (not passGas[ig]) continue;

      // Exclude short straws from most plots
      /*
      if (isShortStraw) {
        if ( trackP < 50 * TRT::GeV ) {
          histoStore()->fillTProfile( "h_pHT"   + gasTypes[ig] + TRTparts[TRTpart] + "_SL", SL, isHT,   w );
          histoStore()->fillTProfile( "h_pHTMB" + gasTypes[ig] + TRTparts[TRTpart] + "_SL", SL, isHTMB, w );
        }
        continue; // Don't do the rest
      }
      */
      
      // HT Probability
      suffix = gasTypes[ig] + TRTparts[TRTpart];
      hname = "h_pHT" + suffix;
      histoStore()->fillTProfile( hname+ "_GF", GF, isHT, w );
      if ( trackP < 50 * TRT::GeV ) {
        histoStore()->fillTProfile( hname+"_AVG", 0., isHT, w );
        histoStore()->fillTProfile( hname+ "_SL", SL, isHT, w );
        histoStore()->fillTProfile( hname+ "_ZR", ZR, isHT, w );
        histoStore()->fillTProfile( hname+ "_TW", TW, isHT, w );
        histoStore()->fillTProfile( hname+ "_OT", OT, isHT, w );
      }
      
      // Middle Bit HT Probability
      hname = "h_pHTMB" + suffix;
      histoStore()->fillTProfile( hname+ "_GF", GF, isHTMB, w );
      if ( trackP < 50 * TRT::GeV ) {
        histoStore()->fillTProfile( hname+"_AVG", 0., isHTMB, w );
        histoStore()->fillTProfile( hname+ "_SL", SL, isHTMB, w );
        histoStore()->fillTProfile( hname+ "_ZR", ZR, isHTMB, w );
        histoStore()->fillTProfile( hname+ "_TW", TW, isHTMB, w );
        histoStore()->fillTProfile( hname+ "_OT", OT, isHTMB, w );
      }
     
      // 2D onset curves
      histoStore()->fillTH2F( "h2_nLT" + suffix + "_GF_OT", GF, OT, w );
      if (isHT)   histoStore()->fillTH2F( "h2_nHT"   + suffix + "_GF_OT", GF, OT, w );
      if (isHTMB) histoStore()->fillTH2F( "h2_nHTMB" + suffix + "_GF_OT", GF, OT, w );
    }
    
    // TEMPORARY: Test correction factors in new version of LH
    if ( (LH()->Corr_el_TW( TRTpart, TW ) / LH2()->Corr_el_TW( TRTpart, TW ) - 1.0) > 0.000005 ) 
      std::cerr << " TW el mismatch!!!    " << LH()->Corr_el_TW(TRTpart,TW) << "  " << LH2()->Corr_el_TW(TRTpart,TW) << std::endl; 
    if ( (LH()->Corr_mu_TW( TRTpart, TW ) / LH2()->Corr_mu_TW( TRTpart, TW ) - 1.0) > 0.000005 ) 
      std::cerr << " TW mu mismatch!!!    " << LH()->Corr_mu_TW(TRTpart,TW) << "  " << LH2()->Corr_mu_TW(TRTpart,TW) << std::endl; 
    
    if ( (LH()->Corr_el_SL( TRTpart, SL ) / LH2()->Corr_el_SL( TRTpart, SL ) - 1.0) > 0.000005 ) 
      std::cerr << " SL el mismatch!!!    " << LH()->Corr_el_SL(TRTpart,SL) << "  " << LH2()->Corr_el_SL(TRTpart,SL) << std::endl; 
    if ( (LH()->Corr_mu_SL( TRTpart, SL ) / LH2()->Corr_mu_SL( TRTpart, SL ) - 1.0) > 0.000005 ) 
      std::cerr << " SL mu mismatch!!!    " << LH()->Corr_mu_SL(TRTpart,SL) << "  " << LH2()->Corr_mu_SL(TRTpart,SL) << std::endl; 
    
    if ( (LH()->Corr_el_ZR( TRTpart, ZR ) / LH2()->Corr_el_ZR( TRTpart, ZR ) - 1.0) > 0.000005 ) 
      std::cerr << " ZR el mismatch!!!    " << LH()->Corr_el_ZR(TRTpart,ZR) << "  " << LH2()->Corr_el_ZR(TRTpart,ZR) << std::endl; 
    if ( (LH()->Corr_mu_ZR( TRTpart, ZR ) / LH2()->Corr_mu_ZR( TRTpart, ZR ) - 1.0) > 0.000005 ) 
      std::cerr << " ZR mu mismatch!!!    " << LH()->Corr_mu_ZR(TRTpart,ZR) << "  " << LH2()->Corr_mu_ZR(TRTpart,ZR) << std::endl; 
    
  }

  // Bin detector parts by |eta| 
  int etaBin5 = -999;
  if      (fabs(track->eta()) < 0.625) etaBin5 = 0;
  else if (fabs(track->eta()) < 1.070) etaBin5 = 1;
  else if (fabs(track->eta()) < 1.304) etaBin5 = 2;
  else if (fabs(track->eta()) < 1.752) etaBin5 = 3;
  else if (fabs(track->eta()) < 2.000) etaBin5 = 4;

  // Calculate electron probabilities
  double probEl[8];
  for( int i(0); i < 8; i++ ) {
    probEl[i] = LH_El[i] / ( LH_El[i] + LH_Mu[i] );
    
  // Plot electron probabilities
    TString hname = TString::Format( "h3_eProb%d_OT", i );
    histoStore()->fillTH3F( hname, etaBin5, probEl[i], OT, w ); 
  }

  // dE/dx distributiosn
  
  // make plots with fraction HT (should I bother with this?) 
  histoStore()->fillTH1F(   "h_nHT_Ar",   nHT_Ar, w );
  histoStore()->fillTH1F( "h_nHTMB_Ar", nHTMB_Ar, w );
  histoStore()->fillTH1F(      "h_nHT",      nHT, w );
  histoStore()->fillTH1F(    "h_nHTMB",    nHTMB, w );

}
