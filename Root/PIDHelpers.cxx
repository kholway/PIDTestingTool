#include "PIDTestingTool/PIDHelpers.h"

namespace TRT {
  
  int getStrawLayer( int part, int module, int strawlayer ) {
  //--------------------------------------------------------------------------------------------------------
    int SL(-999);

    // BARREL: 
    if (part == 0) {
      if (module == 0)       SL = strawlayer;
      else if (module == 1)  SL = 19 + strawlayer;
      else                   SL = 19 + 24 + strawlayer;

    // ENDCAP:
    } else {
      if (module < 6) SL = 16*module + strawlayer;
      else SL =  8*(module-6) + strawlayer;
    }

    return SL;
  }


  double getZRPosition( const xAOD::TrackParticle *track, const xAOD::TrackMeasurementValidation *DC ) {
  //--------------------------------------------------------------------------------------------------------
    double ZR(-999.99);

    
    // BARREL 
    if ( abs(DC->auxdata<int>("bec")) == 1 ) { 
      double Xstraw = DC->auxdata< float >("globalX");
      double Ystraw = DC->auxdata< float >("globalY");
      double pos_r = sqrt(Xstraw*Xstraw + Ystraw*Ystraw);
      double pos_z = pos_r * tan(TRT::PiHalf - track->theta()) + track->z0();
      ZR = fabs(pos_z);   // Pass to more global variable!
      if (ZR > 719.99) ZR = 719.99;    // No zpos > 720mm 
    
    // ENDCAP 
    } else {
      double pos_z = DC->auxdata< float >("globalZ");
      double pos_r = (pos_z - track->z0()) / tan(TRT::PiHalf - track->theta());
      ZR = fabs(pos_r);   // Pass to more global variable!
      if (ZR <  640.01) ZR =  640.01;  // No rpos <  640mm
      if (ZR > 1139.99) ZR = 1139.99;  // No rpos > 1140mm

    }

    return ZR;
  }


  double getDilepMass( xAOD::IParticleContainer lep ) { 
  //--------------------------------------------------------------------------
     if ( lep.size() < 2 ) return -999.99;
     return (lep[0]->p4() + lep[1]->p4()).M() * TRT::invGeV;
  }

}
