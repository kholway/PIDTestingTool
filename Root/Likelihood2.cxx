#include "PIDTestingTool/Likelihood2.h"
#include "TRTFramework/TRTIncludes.h"
#include <TSystem.h>

// 
//  NOTES: 
//  
//  Onset fit parameters should not be redefined each time the function is called
//
//  --> A better calibration method would be to update parameters via histograms
//      all parameters should be stored in histograms and loaded in initialize()
//
//  --> Getting correction factors from histogram would be very good also (same TFile or separate?)
//
//  --> This will make it easy to load calibrations from GSF tracks or data 
//
//  --> I need to go through and make sure all steps match the athena PID tool
//       e.g. tracks without parameters get ZR bin == 0, we do not do this...
//      
//  --> Will need a quick script which can output calibration histograms quickly and easily
//
//  A helper function to return permutations of correction factors would be really helpful
//     helper function can use a bit code to turn on and off corrections
//
//       0:   0 0 0 
//       1:   0 0 1
//       2:   0 1 0
//       3:   0 1 1
//       4:   1 0 0
//       5:   1 0 1
//       6:   1 1 0
//       7:   1 1 1


TH1* Likelihood2::getTH1( TFile *f, TString hname )
{
  TH1 *h = (TH1*)f->Get(hname);
  if ( h == nullptr ) TRT::fatal(" Likelihood :: Failed to load calibration histogram "+hname);
  return h;
}


EL::StatusCode Likelihood2::initialize( TRT::Config &config )
{
  TString calibFile = config.getStr("LH.CalibFile");
  calibFile.ReplaceAll(" ","");
  
  TFile *fc = new TFile( (TString)PathResolverFindCalibFile(calibFile.Data()) );
  //TFile *fc = new TFile("~jgv7/trt_analysis/packages/PIDTestingTool/data/calib_data12.root");
  
  TString parts[3] = {"BRL","ECA","ECB"};
  for( int i(0); i < 3; i++ ) {
    // Prepare Onset Curves
    TH1 *h_onsetXe = (TH1*)fc->Get("ONSET_XE_"+parts[i]);
    TH1 *h_onsetAr = (TH1*)fc->Get("ONSET_AR_"+parts[i]);
    for( int j(0); j < 10; j++ ) {
      onsetPar_Xe[i][j] = h_onsetXe->GetBinContent(j);
      onsetPar_Ar[i][j] = h_onsetAr->GetBinContent(j);
    }

    // Load Correction Factors
    CALIB_EL_SL[i] = getTH1( fc, "CALIB_EL_"+parts[i]+"_SL" );
    CALIB_EL_TW[i] = getTH1( fc, "CALIB_EL_"+parts[i]+"_TW" );
    CALIB_EL_ZR[i] = getTH1( fc, "CALIB_EL_"+parts[i]+"_ZR" );
    
    CALIB_MU_SL[i] = getTH1( fc, "CALIB_MU_"+parts[i]+"_SL" );
    CALIB_MU_TW[i] = getTH1( fc, "CALIB_MU_"+parts[i]+"_TW" );
    CALIB_MU_ZR[i] = getTH1( fc, "CALIB_MU_"+parts[i]+"_ZR" );
  }
 
  return EL::StatusCode::SUCCESS; 
}


double Likelihood2::pHTvP( int TRTpart, double gamma, double occ, int LHtype ) {
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // ---------------------------------------------------------------------
  
  // grab proper parameters
  double *par = onsetPar_Xe[TRTpart];
  if (LHtype == 1) par = onsetPar_Ar[TRTpart]; 
  
  double par1 = par[1] + par[6]*occ;
  double par4 = par[4] + par[7]*occ;
  
  // TR onset part (main part):
  double exp_term = exp(-(log10(gamma) - par4)/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(gamma) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(gamma) - par1);
  
  double pHT_OccZero; 
  if (log10(gamma)      < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(gamma) > par1 )  pHT_OccZero = pHT_HG; 
  else                            pHT_OccZero = pHT_TR;

  double OccFactor = par[8]*occ + par[9]*sqr(occ);
  return pHT_OccZero + (1-pHT_OccZero)*(OccFactor);
}

double Likelihood2::getCorrection( TH1* h, double val ) {
  int ibin = h->FindBin( val );
  if ( ibin > h->GetNbinsX() ) {
    ibin = h->GetNbinsX();
    //Error("ERROR:: Value is outside of range for"+(TString)h->GetTitle())
  }
  return h->GetBinContent( ibin );
}


// Correction Factors Turned on an off by bit code.
//     From MSB to LSB:   ZR  TW  SL 

double Likelihood2::getCorrectionEl( int part, int SL, double TW, double ZR, int code ) {
// -----------------------------------------------------------------------------------------------
  double CF(1.0);
  if ( code & 1 ) CF *= Corr_el_SL( part, SL ); 
  if ( code & 2 ) CF *= Corr_el_TW( part, TW ); 
  if ( code & 4 ) CF *= Corr_el_ZR( part, ZR ); 
  return CF;
}


double Likelihood2::getCorrectionMu( int part, int SL, double TW, double ZR, int code ) {
// -----------------------------------------------------------------------------------------------
  double CF(1.0);
  if ( code & 1 ) CF *= Corr_mu_SL( part, SL ); 
  if ( code & 2 ) CF *= Corr_mu_TW( part, TW ); 
  if ( code & 4 ) CF *= Corr_mu_ZR( part, ZR ); 
  return CF;
}



