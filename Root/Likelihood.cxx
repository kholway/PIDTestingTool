#include "PIDTestingTool/Likelihood.h"
#include <TSystem.h>


  double Likelihood::pHTvP( int TRTpart, double p, double mass, double occ, int LHtype ) {
    // The onset function describes the gamma dependency at occupancy = 0:
    // Parameters:
    //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
    //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
    // -------------------------------------------------------------------
    // LH Type:
    //   0: Xe    
    //   1: Ar    
    //   2: Xe (DATA Calibration) -- NEEDED !!!!!    
    //   3: Ar (DATA Calibration) -- NEEDED !!!!!    
    // -------------------------------------------------------------------
    // TR onset part (main part):
    double gamma = p/mass; //sqrt(p*p + mass*mass)/mass;
    
    double onsetPar_Xe[3][10] = {
      { 1.0000, 3.7204, 0.0260, 0.1445, 3.0461, 0.2206, 0.0000, 0.0078, 0.0918, 0.0744},  // Barrel  Prob: 0.9992 
      { 1.0000, 3.5836, 0.0468, 0.1475, 3.0943, 0.1303, 0.0000, 0.0089, 0.1054, 0.0472},  // EndcapA  Prob: 1.0000 
      { 1.0000, 3.4798, 0.0433, 0.1824, 3.0730, 0.1244, 0.0000, 0.0300, 0.1007, 0.1261}}; // EndcapB  Prob: 0.8536
  
    double onsetPar_Ar[3][10] = {
      { 1.0000, 2.8342, 0.0384, 0.0185, 2.7161, 0.0366, 0.0000, 0.0013, 0.1261, 0.1241},  // Barrel  Prob: 1.0000 
      { 1.0000, 3.2551, 0.0388, 0.0338, 2.9090, 0.1663, 0.0000, 0.1604, 0.1100, 0.0521},  // EndcapA  Prob: 0.9970
      { 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000}}; // EndcapB  -----------
  
    double *par = onsetPar_Xe[TRTpart];
    if (LHtype == 1) par = onsetPar_Ar[TRTpart]; 
    
    double par1 = par[1] + par[6]*occ;
    double par4 = par[4] + par[7]*occ;
    //double par4 = par[4] + par[9]*occ; //JV, broken on purpose.
    
    // TR onset part (main part):
    double exp_term = exp(-(log10(gamma) - par4)/par[5]);
    double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

    // dE/dx part (linear at low gamma):
    double exp_term0 = exp(-(par[0] - par4)/par[5]);
    double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
    double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
    double pHT_dEdx = alpha0 + beta0*(log10(gamma) - par[0]);

    // High-gamma part (linear at high gamma):
    double exp_term1 = exp(-(par1 - par4)/par[5]);
    double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
    double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
    double pHT_HG   = alpha1 + beta1*(log10(gamma) - par1);
    
    double pHT_OccZero; 
    if (log10(gamma)      < par[0]) pHT_OccZero = pHT_dEdx;
    else if (log10(gamma) > par1 ) pHT_OccZero = pHT_HG; //JV
    else                           pHT_OccZero = pHT_TR;
    //else if (log10(gamma) > par[1]) pHT_OccZero = pHT_HG;
  
    double OccFit = par[2] + par[8]*occ + par[9]*sqr(occ);
    double pHT = pHT_OccZero + (1-pHT_OccZero)*(OccFit - par[2]);
    return pHT;
  }


  // ------------------------------------------------------------------------------------------------------------ //
  // Parametrization of pHT for electrons and muons, determined from 500k MC samples of Zee and Zmumu:
  // ------------------------------------------------------------------------------------------------------------ //
  
  // -------------------------------------------------------- //
  // Electrons:
  // -------------------------------------------------------- //

  // Straw Layer (SL):
  double Likelihood::Corr_el_SL(int part, double SL){
    SLbin = int(SL);
    if (part == 0) {
      if (SLbin < 0 || SLbin > 72) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_B_Zee_SL[SLbin];
    }
    if (part == 1) {
      if (SLbin < 0 || SLbin > 95) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EA_Zee_SL[SLbin];
    }
    if (part == 2) {
      if (SLbin < 0 || SLbin > 63) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EB_Zee_SL[SLbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Straw Position (ZR - Z in Barrel, R in Endcaps):
  double Likelihood::Corr_el_ZR(int part, double ZR){
    if (part == 0) {
      ZRbin = int(ZR/720.0*36.0);
      if (ZRbin < 0 || ZRbin > 35) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_B_Zee_ZR[ZRbin];
    }
    if (part == 1) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EA_Zee_ZR[ZRbin];
    }
    if (part == 2) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EB_Zee_ZR[ZRbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Radius of Track (TW):
  double Likelihood::Corr_el_TW(int part, double TW){
    TWbin = int(TW/2.2*44.0);
    if (TWbin > 43) TWbin = 43;

    if (part == 0) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_B_Zee_TW[TWbin];
    }
    if (part == 1) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EA_Zee_TW[TWbin];
    }
    if (part == 2) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EB_Zee_TW[TWbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Occupancy Regional (OR) - as a number in the range [0,1]:
  double Likelihood::Corr_el_OR(int part, double OR){
    ORbin = int(OR/2.0);
    if (ORbin > 49) ORbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) {
      
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_B_Zee_OR[ORbin];
    }
    if (part == 1) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EA_Zee_OR[ORbin];
    }
    if (part == 2) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EB_Zee_OR[ORbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
  
  // Track Occupancy (OT) - as a number in the range [0,1]:
  double Likelihood::Corr_el_OT(int part, double OT){
    OTbin = int(OT/2.0*100);
    if (OTbin > 49) OTbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) { 
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_B_Zee_OT[OTbin];
    }
    if (part == 1) {
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_EA_Zee_OT[OTbin];
    }
    if (part == 2) {
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_EB_Zee_OT[OTbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
  
// Straw Layer (SL):
  double Likelihood::Corr_elMC_SL(int part, double SL){
    SLbin = int(SL);
    if (part == 0) {
      if (SLbin < 0 || SLbin > 72) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_B_ZeeMC_SL[SLbin];
    }
    if (part == 1) {
      if (SLbin < 0 || SLbin > 95) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EA_ZeeMC_SL[SLbin];
    }
    if (part == 2) {
      if (SLbin < 0 || SLbin > 63) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EB_ZeeMC_SL[SLbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Straw Position (ZR - Z in Barrel, R in Endcaps):
  double Likelihood::Corr_elMC_ZR(int part, double ZR){
    if (part == 0) {
      ZRbin = int(ZR/720.0*36.0);
      if (ZRbin < 0 || ZRbin > 35) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_B_ZeeMC_ZR[ZRbin];
    }
    if (part == 1) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EA_ZeeMC_ZR[ZRbin];
    }
    if (part == 2) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EB_ZeeMC_ZR[ZRbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Radius of Track (TW):
  double Likelihood::Corr_elMC_TW(int part, double TW){
    TWbin = int(TW/2.2*44.0);
    if (TWbin > 43) TWbin = 43;

    if (part == 0) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_B_ZeeMC_TW[TWbin];
    }
    if (part == 1) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EA_ZeeMC_TW[TWbin];
    }
    if (part == 2) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EB_ZeeMC_TW[TWbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Occupancy Regional (OR) - as a number in the range [0,1]:
  double Likelihood::Corr_elMC_OR(int part, double OR){
    ORbin = int(OR/2.0);
    if (ORbin > 49) ORbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) {
      
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_B_ZeeMC_OR[ORbin];
    }
    if (part == 1) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EA_ZeeMC_OR[ORbin];
    }
    if (part == 2) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EB_ZeeMC_OR[ORbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
  

  // -------------------------------------------------------- //
  // Muons:
  // -------------------------------------------------------- //

  // Straw Layer (SL):
  double Likelihood::Corr_mu_SL(int part, double SL){
    SLbin = int(SL);
    if (part == 0) {
      if (SLbin < 0 || SLbin > 72) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_B_Zmm_SL[SLbin];
    }
    if (part == 1) {
      if (SLbin < 0 || SLbin > 95) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EA_Zmm_SL[SLbin];
    }
    if (part == 2) {
      if (SLbin < 0 || SLbin > 63) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EB_Zmm_SL[SLbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Straw Position (ZR - Z in Barrel, R in Endcaps):
  double Likelihood::Corr_mu_ZR(int part, double ZR){
    if (part == 0) {
      ZRbin = int(ZR/720.0*36.0);
      if (ZRbin < 0 || ZRbin > 35) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_B_Zmm_ZR[ZRbin];
    }
    if (part == 1) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EA_Zmm_ZR[ZRbin];
    }
    if (part == 2) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EB_Zmm_ZR[ZRbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Radius of Track (TW):
  double Likelihood::Corr_mu_TW(int part, double TW){
    TWbin = int(TW/2.2*44.0);
    if (TWbin > 43) TWbin = 43;

    if (part == 0) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_B_Zmm_TW[TWbin];
    }
    if (part == 1) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EA_Zmm_TW[TWbin];
    }
    if (part == 2) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EB_Zmm_TW[TWbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Occupancy Regional (OR):
  double Likelihood::Corr_mu_OR(int part, double OR){
    ORbin = int(OR/2.0);
    if (ORbin > 49) ORbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_B_Zmm_OR[ORbin];
    }
    if (part == 1) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EA_Zmm_OR[ORbin];
    }
    if (part == 2) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EB_Zmm_OR[ORbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
  
  // Track Occupancy (OT) - as a number in the range [0,1]:
  double Likelihood::Corr_mu_OT(int part, double OT){
    OTbin = int(OT/2.0*100);
    if (OTbin > 49) OTbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) { 
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_B_Zmm_OT[OTbin];
    }
    if (part == 1) {
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_EA_Zmm_OT[OTbin];
    }
    if (part == 2) {
      if (OTbin < 0 || OTbin > 49) printf("  ERROR: OTbin = %5d \n", OTbin); else return CpHT_EB_Zmm_OT[OTbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
  
  // Straw Layer (SL):
  double Likelihood::Corr_muMC_SL(int part, double SL){
    SLbin = int(SL);
    if (part == 0) {
      if (SLbin < 0 || SLbin > 72) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_B_ZmmMC_SL[SLbin];
    }
    if (part == 1) {
      if (SLbin < 0 || SLbin > 95) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EA_ZmmMC_SL[SLbin];
    }
    if (part == 2) {
      if (SLbin < 0 || SLbin > 63) printf("  ERROR: SLbin = %5d \n", SLbin); else return CpHT_EB_ZmmMC_SL[SLbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Straw Position (ZR - Z in Barrel, R in Endcaps):
  double Likelihood::Corr_muMC_ZR(int part, double ZR){
    if (part == 0) {
      ZRbin = int(ZR/720.0*36.0);
      if (ZRbin < 0 || ZRbin > 35) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_B_ZmmMC_ZR[ZRbin];
    }
    if (part == 1) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EA_ZmmMC_ZR[ZRbin];
    }
    if (part == 2) {
      ZRbin = int((ZR-630.0)/400.0*40.0);
      if (ZRbin < 0 || ZRbin > 39) printf("  ERROR: ZRbin = %5d \n", ZRbin); else return CpHT_EB_ZmmMC_ZR[ZRbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Radius of Track (TW):
  double Likelihood::Corr_muMC_TW(int part, double TW){
    TWbin = int(TW/2.2*44.0);
    if (TWbin > 43) TWbin = 43;
    
    if (part == 0) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_B_ZmmMC_TW[TWbin];
    }
    if (part == 1) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EA_ZmmMC_TW[TWbin];
    }
    if (part == 2) {
      if (TWbin < 0 || TWbin > 43) printf("  ERROR: TWbin = %5d \n", TWbin); else return CpHT_EB_ZmmMC_TW[TWbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }


  // Occupancy Regional (OR):
  double Likelihood::Corr_muMC_OR(int part, double OR){
    ORbin = int(OR/2.0);
    if (ORbin > 49) ORbin = 49;       // NOTE: Simply put a maximum by hand (to avoid multiple printouts)!!!
    if (part == 0) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_B_ZmmMC_OR[ORbin];
    }
    if (part == 1) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EA_ZmmMC_OR[ORbin];
    }
    if (part == 2) {
      if (ORbin < 0 || ORbin > 49) printf("  ERROR: ORbin = %5d \n", ORbin); else return CpHT_EB_ZmmMC_OR[ORbin];
    }
    printf("  ERROR: Either part is not in [0,2] or binning is wrong! \n");
    return 1.0;
  }
